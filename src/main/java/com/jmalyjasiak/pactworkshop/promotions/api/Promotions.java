package com.jmalyjasiak.pactworkshop.promotions.api;

import java.util.List;

public record Promotions(
        List<Promotion> promotions
) {
}
