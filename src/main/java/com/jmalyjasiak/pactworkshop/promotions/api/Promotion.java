package com.jmalyjasiak.pactworkshop.promotions.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collection;

public record Promotion(
        String promotionId,
        Reward reward,
        Trigger trigger,
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'", timezone = "UTC")
        Instant activeFrom,
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'", timezone = "UTC")
        Instant createdAt
) {
    record CashReward(Money discount) implements Reward {
        @Override
        public String type() {
            return "CASH";
        }
    }

    record PercentReward(BigDecimal discount) implements Reward {
        @Override
        public String type() {
            return "PERCENT";
        }
    }

    record Trigger(Collection<String> productIds, Money moneySpend) {}

    @JsonTypeInfo(
            visible = true,
            use = JsonTypeInfo.Id.NAME,
            property = "type",
            include = JsonTypeInfo.As.EXISTING_PROPERTY
    )
    interface Reward {

        @JsonProperty("type")
        String type();
    }
}
