package com.jmalyjasiak.pactworkshop.promotions.api;

import java.math.BigDecimal;

public record Money(String currency, BigDecimal amount) {

    Money(String currency, String amount) {
        this(currency, new BigDecimal(amount));
    }
}
