package com.jmalyjasiak.pactworkshop.promotions.api;

import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import au.com.dius.pact.provider.junitsupport.loader.PactBrokerConsumerVersionSelectors;
import au.com.dius.pact.provider.junitsupport.loader.SelectorBuilder;
import au.com.dius.pact.provider.spring.spring6.PactVerificationSpring6Provider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@Provider("promotions")
@Tag("pact-provider-test")
@PactBroker(enablePendingPacts = "true", providerBranch = "main")
public class OffersContractVerificationTest {

    @LocalServerPort
    private int localServerPort;

    @Autowired
    PromotionsRepository promotionsRepository;

    @TestTemplate
    @ExtendWith(PactVerificationSpring6Provider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @PactBrokerConsumerVersionSelectors
    public static SelectorBuilder consumerVersionSelectors() {
        return new SelectorBuilder()
                .branch("main")
                .deployedOrReleased();
    }

    @BeforeEach
    void before(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", localServerPort, "/"));
    }

    @State("cash promotion exists")
    void cashPromotionExists(Map<String, String> params) {
        promotionsRepository.savePromotion(new Promotion(
                params.get("promotionId"),
                new Promotion.CashReward(new Money("USD","10.00")),
                new Promotion.Trigger(
                        List.of("other-promotion-id"),
                        new Money("USD","100.00")),
                Instant.now(),
                Instant.now()));
    }

    @State("percent promotion exists")
    void percentPromotionExists(Map<String, String> params) {
        promotionsRepository.savePromotion(new Promotion(
                params.get("promotionId"),
                new Promotion.PercentReward(new BigDecimal("123")),
                new Promotion.Trigger(
                        List.of("other-promotion-id"),
                        new Money("USD","100.00")),
                Instant.now(),
                Instant.now()));
    }
}
